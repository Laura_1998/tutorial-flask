from flask import Flask

app = Flask(__name__)

#lista que almacenará los posts que se guarden
posts = []

@app.route('/hello')
def hello_world():
    return 'Hello world'

#funcion que mostrará los post del blog
@app.route('/')
def  index():
    return "{} posts".format(len(posts)) #muestra en el navegador el numero de posts que tiene la variable

#funcion que muestra el detalle de un post
#se define el parametro slug en la URL
@app.route("/p/<string:slug>/") #<converter:param> 
def show_post(slug):
    return "mostrando el post {}".format(slug)

# Creando la vista para crear/modificar un post
@app.route("/admin/post/")
@app.route("/admin/post/<int:post_id>")
def post_form(post_id=None):
    return "post_form {}".format(post_id)
