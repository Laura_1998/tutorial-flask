''' https://www.youtube.com/watch?v=IgCfZkR8wME&t=1609s '''

from flask import Flask, render_template, request, redirect, url_for, flash
from flask_mysqldb import MySQL

app = Flask(__name__)

#datos para la conexión a la base de datos
app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = '3053559355'
app.config['MYSQL_DB'] = 'contacts'

mysql = MySQL(app)

#settings
#se inicializa una sesión guardandola en la memoria de la aplicación
#secret_key para indicar como estará protegida nuestra sesión
app.secret_key = 'mysecretkey'
@app.route('/')
def main():
    #Se realiza consulta para mostrar datos en la tabla creada en html
    cur = mysql.connection.cursor()
    cur.execute('SELECT * FROM table_contacts')
    data = cur.fetchall()
    return render_template('index.html', contacts = data)

@app.route('/add', methods =['POST'])
def add_contact():
    if request.method == 'POST':
        fullname = request.form['fullname']
        phone = request.form['phone']
        email = request.form['email']
    #obtenemos la conexión
        cursor = mysql.connection.cursor()
    #escribimos la consulta
        cursor.execute('INSERT INTO table_contacts (fullname, phone, email) VALUES (%s,%s,%s)', (fullname,phone,email))
    #ejecutamos la consulta
        mysql.connection.commit()
    #el método flash sirve para enviar un mensaje, para verlo hay que hacer una comprobación en la
    #la vista (html)
    flash('Contact added correctly')
    #se utiliza el método redirect para redirigir a otra ruta, en el metodo url_for se indica el
    #nombre de la función que esta asociada a la ruta principal 
    return redirect(url_for('main'))
        
@app.route('/update')
def update_contact():
    return 'Updating contact'

@app.route('/delete')
def delete_contact():
    return 'delete contact'

#si el archivo que se está ejecutando como principal es app.py se ejecutará el servidor
if __name__ == '__main__':
    app.run(debug=True, port=4000)